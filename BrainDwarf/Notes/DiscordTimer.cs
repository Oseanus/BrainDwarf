﻿using System;
using System.Threading;
using Notes.Data;

namespace Notes
{
    /// <summary>
    /// Timer class to schedule through the notes
    /// </summary>
    public class DiscordTimer
    {
        #region fields
        private NotesInfo _notes;
        private bool _isRunning;

        /// <summary>
        /// Shows if the timer is running or not.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
        }
        #endregion

        #region ctor
        public DiscordTimer()
        {
            _notes = new NotesInfo();
            _isRunning = false;
        }
        #endregion

        #region event handler
        /// <summary>
        /// Event handler for the expired date.
        /// </summary>
        public event EventHandler<OnExpirationDateEventArgs> OnDateExpired;
        #endregion

        #region methods
        /// <summary>
        /// Adds a note to _notes.
        /// </summary>
        /// <param name="serverID">The server id of the discord server.</param>
        /// <param name="channelID">The id of the channel in which the note was created.</param>
        /// <param name="userID">The id of the user who created the note.</param>
        /// <param name="message">The actual message of the note.</param>
        /// <param name="expirationDate">The date when the note shall expire.</param>
        public void AddNote(
            string serverID,
            string channelID,
            string userID,
            string message,
            DateTime expirationDate)
        {
            var discord = new Discord(serverID, channelID, userID);
            var note = new Note(message, expirationDate, discord);

            _notes.Add(note);
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        public void StartTimer()
        {
            _isRunning = true;

            while (_isRunning)
            {
                // Stop timer if _notes is empty
                if (_notes.Count == 0)
                {
                    StopTimer();
                    return;
                }

                // Check all note for each iteration.
                CheckNotes();

                // Wait for a minute.
                Thread.Sleep(6000);
            }
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void StopTimer()
        {
            _isRunning = false;
        }

        /// <summary>
        /// Checks for all notes if the expiration date is reached.
        /// If this is the case the OnDateExpired is fired.
        /// </summary>
        private void CheckNotes()
        {
            for(int i = 0; i < _notes.Count; i++)
            {
                bool isExpired = CheckSingleNote(_notes[i]);

                if (isExpired)
                    _notes.Remove(i);
            }
        }

        /// <summary>
        /// Fires the OnDateExpired if date is expired.
        /// </summary>
        /// <param name="note"></param>
        /// <returns>If date expired return true otherwise false.</returns>
        private bool CheckSingleNote(Note note)
        {
            if(DateTime.Compare(DateTime.Now, note.ExpirationDate) >= 0)
            {
                Discord discord = note.DiscordInfo;

                // Construct the event arguments
                var args = new OnExpirationDateEventArgs
                {
                    Message = note.Message,
                    ServerID = discord.ServerID,
                    ChannelID = discord.ChannelID,
                    UserID = discord.UserID
                };

                // Fire event
                OnDateExpired.Invoke(this, args);

                return true;
            }

            return false;
        }
        #endregion

        #region internal classes
        /// <summary>
        /// Event arguments for OnDateExpired.
        /// </summary>
        public class OnExpirationDateEventArgs : EventArgs
        {
            public string Message { get; set; }
            public string ServerID { get; set; }
            public string ChannelID { get; set; }
            public string UserID { get; set; }
        }
        #endregion
    }
}