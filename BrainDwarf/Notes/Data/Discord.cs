﻿namespace Notes.Data
{
    /// <summary>
    /// Holds necessary information about Discord.
    /// </summary>
    internal class Discord
    {
        #region fields
        private string _serverID;
        private string _channelID;
        private string _userID;

        /// <summary>
        /// Returns the ID of the discord server.
        /// </summary>
        public string ServerID
        {
            get
            {
                return _serverID;
            }
        }

        /// <summary>
        /// Returns the channel id.
        /// </summary>
        public string ChannelID
        {
            get
            {
                return _channelID;
            }
        }

        public string UserID
        {
            get
            {
                return _userID;
            }
        }
        #endregion

        #region ctor
        public Discord(string serverID, string channelID, string userID)
        {
            _serverID = serverID;
            _channelID = channelID;
            _userID = userID;
        }
        #endregion
    }
}