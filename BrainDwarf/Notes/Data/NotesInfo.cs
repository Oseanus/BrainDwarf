﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notes.Data
{
    /// <summary>
    /// Notes container class for a note collection.
    /// </summary>
    internal class NotesInfo
    {
        #region fields
        private List<Note> _notes;

        /// <summary>
        /// Returns a note dependend from the index.
        /// </summary>
        /// <param name="i">Integer which determins the indes.</param>
        /// <returns>The note which will be returned.</returns>
        public Note this[int i]
        {
            get
            {
                return _notes[i];
            }
        }

        /// <summary>
        /// Returns the number of notes in the list.
        /// </summary>
        public int Count
        {
            get
            {
                return _notes.Count;
            }
        }
        #endregion

        #region ctor
        public NotesInfo()
        {
            _notes = new List<Note>();
        }
        #endregion

        #region methods
        /// <summary>
        /// Add a note to the notes collection.
        /// </summary>
        /// <param name="note">The added note.</param>
        public void Add(Note note)
        {
            _notes.Add(note);
        }

        /// <summary>
        /// Removes an expired note.
        /// </summary>
        /// <param name="i">The index which determins the position of the note.</param>
        public void Remove(int i)
        {
            _notes.RemoveAt(i);
        }
        #endregion
    }
}