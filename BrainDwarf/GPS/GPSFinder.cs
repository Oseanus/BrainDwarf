﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPS
{
    public static class GPSFinder
    {
        #region fields
        private static string mapsAdress = "https://maps.google.com/maps/place/";
        private static string staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap?center=";
        private static string staticMapLocation = ",CA&zoom=14&zoom=30&size=800x400&format=jpeg";
        private static string staticMapMarkers = "&markers=";
        private static string staticMapKey = "&key=###############################";
        #endregion

        #region methods
        /// <summary>
        /// Generates a Google Maps link for the searched location.
        /// </summary>
        /// <param name="location">The seach parameter.</param>
        /// <returns>The final Google Maps link.</returns>
        public static string SearchLocation(string location)
        {
            string result = MergeString(location.Trim());

            string link = mapsAdress + result;
            return link;
        }

        /// <summary>
        /// Generates a static map from Google Maps.
        /// </summary>
        /// <param name="location">Search parameter for the location.</param>
        /// <returns>The link to the static map.</returns>
        public static string PostStaticMap(string location)
        {
            string result = MergeString(location.Trim());

            string staticMap = staticMapUrl +
                               result + staticMapLocation +
                               staticMapMarkers + result +
                               staticMapKey;
            return staticMap;
        }

        /// <summary>
        /// If the string has spaces such as New York, this method merges + resulting New+York and returns it.
        /// Otherwise return location.
        /// </summary>
        /// <param name="location">The location which should be displayed in Google Maps.</param>
        /// <returns>The final string for the link.</returns>
        private static string MergeString(string location)
        {
            string[] splittedString = location.Split(' ');

            if(splittedString.Length > 1)
            {
                string result = "";

                foreach(string s in splittedString)
                {
                    result = result + s + "+";
                }

                result = result.Substring(0, result.Length - 1);

                return result.Trim();
            }

            return location;
        }
        #endregion
    }
}