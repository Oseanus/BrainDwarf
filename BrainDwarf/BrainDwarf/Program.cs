﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DSharpPlus;
using GPS;
using Notes;

namespace BrainDwarf
{
    class Program
    {
        #region fields
        private static DiscordTimer timer;
        #endregion

        #region methods
        /// <summary>
        /// Run the client.
        /// </summary>
        /// <returns>Asynchrounous for void.</returns>
        public static async Task Run()
        {
            // Create discord client
            var discord = new DiscordClient(new DiscordConfig
            {
                AutoReconnect = true,
                DiscordBranch = Branch.Stable,
                LargeThreshold = 250,
                LogLevel = LogLevel.Info,
                Token = "MzI5MjQ0Mjg0NDg4Mzg0NTI0.DDPsJw.VBKAAan9rgg_EEhNdP4Ic0hqU5Q",
                TokenType = TokenType.Bot,
                UseInternalLogHandler = false
            });

            // Set timer for notes
            timer = new DiscordTimer();
            timer.OnDateExpired += async (object sender, DiscordTimer.OnExpirationDateEventArgs e) =>
            {
                string message = e.Message;
                ulong channelID = ulong.Parse(e.ChannelID);
                string userID = e.UserID;

                DiscordChannel channel = await discord.GetChannelAsync(channelID);

                await discord.SendMessageAsync(channel, message);
            };

            // Print log infos
            discord.DebugLogger.LogMessageReceived += (o, e) =>
            {
                Console.WriteLine($"[{e.Timestamp}] [{e.Application}] [{e.Level}] {e.Message}");
            };

            // Respond to messeges
            discord.MessageCreated += async e =>
            {
                // Don't respond to other bots
                if (!e.Author.IsBot)
                {
                    int padding = 3;

                    if (e.Message.Content.ToLower() == "ping")
                        await e.Message.RespondAsync("pong");
                    else if (e.Message.Content.ToLower() == "marco")
                        await e.Message.RespondAsync("Polo!");
                    else if (e.Message.Content == "--help")
                        await e.Message.RespondAsync("Following orders are responded.\n" +
                                                     "ping".PadRight(padding) + "=> pong\n" +
                                                     "Marco".PadRight(padding) + "=> Polo!\n" +
                                                     "--user".PadRight(padding) + "=> Your username\n" +
                                                     "--channel".PadRight(padding) + "=> Displays the current channel\n" +
                                                     "--irony".PadRight(padding) + "=> Irony tag\n" +
                                                     "--legendary".PadRight(padding) + "=> Legendary\n" +
                                                     "--find".PadRight(padding) + "=> Responds with a Google Maps link \n" +
                                                     "--note dd/mm/yyyy hh:mm:00 message".PadRight(padding) + "=> Creates a message with expiration date.\n" +
                                                     "--help".PadRight(padding) + "=> This fucking list");
                    else if (e.Message.Content == "--user")
                        await e.Message.RespondAsync(string.Format($"<@{e.Message.Author.Id}>"));
                    else if (e.Message.Content == "--channel")
                        await e.Message.RespondAsync($"You are in channel {e.Channel.Name}.");
                    else if (e.Message.Content.StartsWith("--irony "))
                        await e.Message.RespondAsync($"I case you don't know, {e.Author.Username} is ironic.");
                    else if(e.Message.Content == "--legendary")
                    {
                        await e.Message.RespondAsync("This is legen... Wait for it...");

                        Thread.Sleep(5000);

                        await e.Message.RespondAsync("... dary!");
                    }
                    else if(e.Message.Content.StartsWith("--find"))
                    {
                        string location = e.Message.Content.Substring(7);
                        string result = GPSFinder.SearchLocation(location);
                        string map = GPSFinder.PostStaticMap(location);

                        await e.Message.RespondAsync(result);
                        await e.Message.RespondAsync(map);
                    }
                    else if(e.Message.Content.StartsWith("--note"))
                    {
                        string date = e.Message.Content.Substring(6, 21);
                        DateTime expirationDate = Convert.ToDateTime(date);
                        string message = e.Message.Content.Substring(27);
                        string serverID = "";
                        string channelID = e.Channel.Id.ToString();
                        string userName = e.Message.Author.Username;
                        string userDiscriminator = e.Message.Author.Discriminator;
                        string userID = $"<@{e.Message.Author.Id}>";

                        timer.AddNote(
                            serverID,
                            channelID,
                            userID,
                            message,
                            expirationDate);

                        if(timer.IsRunning == false)
                        {
                            ThreadPool.QueueUserWorkItem(o => timer.StartTimer());
                        }
                    }
                }
            };

            // Respond to words in sentences
            discord.MessageCreated += async e =>
            {
                if(!e.Author.IsBot)
                {
                    if (e.Message.Content.ToLower().Contains("penis"))
                        await e.Message.RespondAsync("Ha ha, penis he said!");
                }
            };

            await discord.ConnectAsync();
            await Task.Delay(-1);
        }
        #endregion

        #region main method
        static void Main(string[] args)
        {
            Run().GetAwaiter().GetResult();
        }
        #endregion
    }
}